//
//  Matrix.h
//  RBM
//
//  Created by Csordás Róbert on 2015. 09. 03..
//  Copyright (c) 2015. Csordás Róbert. All rights reserved.
//

#ifndef __RBM__Matrix__
#define __RBM__Matrix__

#include <stdio.h>
#include "MatrixPayload.h"
#include <fstream>

// The matrix class can be passed cheaply from function to fuction, because
// it's payload only copies when the operation is requires so.
//
// Transposing a matrix doesn't copy any data.
//
// The Matrix() constructor doesn't creates the payload. It is designed for
// uses like Matrix a; if (b) a=c; else a=d; Only the destructor and the
// operator = are valid operations.
//
class Matrix{
    private:
        MatrixPayload *payload;
        bool isTranspose;
    
        void releasePayload();
    public:
        // IndexProxy is used to manage matrix indexing. It works both with
        // normal and transposed matrices.
        //
        // Matrix class uses this internally to. It is not woth of creating
        // an incrementing accumulator to move the index from row to row (or
        // col to col), because both multiplication and addition is equally
        // fast on Intel CPUs.
        class IndexProxy{
            private:
                double *data;
                int stride;
            public:
                IndexProxy(const IndexProxy &other);
                IndexProxy(double *data, int stride);
                double & operator[] (const int nIndex) const;
        };
    
        Matrix();
        Matrix(int nRows, int nCols);
        Matrix(const Matrix &other);
        ~Matrix();
    
        static Matrix Zeros(int nRows, int nCols);
        static Matrix Ones(int nRows, int nCols);
        static Matrix UniformRandom(int nRows, int nCols, double min, double max);
        static Matrix NormalRandom(int nRows, int nCols, double mu, double sigma);
        static Matrix RowVector(int cols);
        static Matrix ColVector(int rows);
    
        Matrix & operator=(const Matrix &other);
    
        Matrix& operator+=(const Matrix& rhs);
        Matrix& operator+=(double rhs);
        Matrix operator+(const Matrix& rhs) const;
        Matrix operator+(double rhs) const;
        friend Matrix operator+(double lhs, const Matrix& rhs);
    
        Matrix& operator-=(const Matrix& rhs);
        Matrix& operator-=(double rhs);
        Matrix operator-(const Matrix& rhs) const;
        Matrix operator-(double rhs) const;
        friend Matrix operator-(double lhs, const Matrix& rhs);
    
        IndexProxy operator[] (const int nIndex) const;
        IndexProxy row(int index) const;
        IndexProxy col(int index) const;
    
        Matrix operator* (const Matrix &other) const;
        Matrix &operator*= (const Matrix &other);
        Matrix operator* (const double other) const;
        Matrix &operator*= (const double other);
    
        Matrix operator/ (const double other) const;
        Matrix &operator/= (const double other);
        friend Matrix operator*(double lhs, const Matrix& rhs);
    
        //Faster than [][]
        double &operator ()(int r, int c);
    
        Matrix transpose() const;
    
        int rows() const;
        int cols() const;
    
        Matrix rowwiseAdd(const Matrix &other) const;
        Matrix colwiseAdd(const Matrix &other) const;
    
        Matrix rowwiseSubtract(const Matrix &other) const;
        Matrix colwiseSubtract(const Matrix &other) const;
    
        Matrix colsum() const;
        Matrix rowsum() const;
    
        Matrix colmean() const;
        Matrix rowmean() const;
    
        void save(std::ofstream &file) const;
        void load(std::ifstream &file);
    
        Matrix elementwise(double (*transform)(double d)) const;
    
        double sum();
    
        void minMax(double &min, double &max) const;
    
};

#endif /* defined(__RBM__Matrix__) */
