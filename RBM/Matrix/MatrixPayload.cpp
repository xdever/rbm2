//
//  MatrixPayload.cpp
//  RBM
//
//  Created by Csordás Róbert on 2015. 09. 03..
//  Copyright (c) 2015. Csordás Róbert. All rights reserved.
//

#include "MatrixPayload.h"
#include "MatrixPool.h"

MatrixPayload::MatrixPayload(int nRows, int nCols){
    this->nRows=nRows;
    this->nCols=nCols;
    this->referenceCounter=1;
    data=new double[nRows*nCols];
    
}
                                              
MatrixPayload::~MatrixPayload(){
    delete [] data;
}
