//
//  Matrix.cpp
//  RBM
//
//  Created by Csordás Róbert on 2015. 09. 03..
//  Copyright (c) 2015. Csordás Róbert. All rights reserved.
//

#include "Matrix.h"
#include "MatrixPool.h"
#include <stdlib.h>
#include <assert.h>
#include <time.h>
#include <random>

//Asserts on dimension mismatch
#define DIMCHECK(a,b) assert(((a)->rows()==(b)->rows()) && ((a)->cols()==(b)->cols()))

//Defines a new matrix with the same size as the current
#define DEFINE_SAME_SIZE_MATRIX(name) Matrix name(rows(), cols())
#define DEFINE_SAME_SIZE_MATRIX_NO_TRANSPOSE(name) Matrix name(payload->nRows, payload->nCols)


Matrix::IndexProxy::IndexProxy(double *data, int stride){
    this->data=data;
    this->stride=stride;
}

Matrix::IndexProxy::IndexProxy(const IndexProxy &other){
    data=other.data;
    stride=other.stride;
}

double & Matrix::IndexProxy::operator[] (const int nIndex) const{
    return data[stride*nIndex];
}

Matrix::Matrix(){
    payload=NULL;
}

Matrix::Matrix(int nRows, int nCols){
    //Allocate a new payload.
    payload=MatrixPool::allocate(nRows, nCols);
}

Matrix Matrix::RowVector(int cols){
    return Matrix(1, cols);
}

Matrix Matrix::ColVector(int rows){
    return Matrix(rows,1);
}


Matrix::Matrix(const Matrix &other){
    (*this)=other;
}

void Matrix::releasePayload(){
    if (!payload)
        return;
    
    //If deallocated, handle the reference counter. If needed,
    //push the payload back in the pool
    payload->referenceCounter--;
    if (payload->referenceCounter==0){
        MatrixPool::deallocate(payload);
    }
}

Matrix::~Matrix(){
    releasePayload();
}

Matrix::IndexProxy Matrix::operator[] (const int nIndex) const{
    if (isTranspose)
        return Matrix::IndexProxy(&payload->data[nIndex], payload->nCols);
    else
        return Matrix::IndexProxy(&payload->data[nIndex*payload->nCols],1);
}

Matrix::IndexProxy Matrix::row(int index) const{
    return (*this)[index];
}

Matrix::IndexProxy Matrix::col(int index) const{
    if (isTranspose)
        return Matrix::IndexProxy(&payload->data[index*payload->nCols],1);
    else
        return Matrix::IndexProxy(&payload->data[index], payload->nCols);
}

Matrix & Matrix::operator=(const Matrix &other){
    //First delete the old payload.
    releasePayload();
    
    //When copying, increase the reference counter.
    this->payload=other.payload;
    payload->referenceCounter++;
    
    this->isTranspose=other.isTranspose;
    
    return *this;
}

Matrix Matrix::Zeros(int nRows, int nCols){
    //Construct a zero matrix.
    Matrix r(nRows, nCols);
    
    const int n=nRows*nCols;
    for (int a=0; a<n; a++){
        r.payload->data[a]=0.0;
    }
    
    return r;
}

Matrix Matrix::Ones(int nRows, int nCols){
    //Construct an all-one matrix
    Matrix r(nRows, nCols);
    
    const int n=nRows*nCols;
    for (int a=0; a<n; a++){
        r.payload->data[a]=1.0;
    }
    
    return r;
}

Matrix Matrix::UniformRandom(int nRows, int nCols, double min, double max){
    //Construct uniform random matrix
    Matrix r(nRows, nCols);
    
    const int n=nRows*nCols;
    for (int a=0; a<n; a++){
        r.payload->data[a]=min + (((double)rand())/((double)RAND_MAX))*(max-min);
    }
    
    return r;
}

Matrix Matrix::NormalRandom(int nRows, int nCols, double mu, double sigma){
    Matrix r(nRows, nCols);
    
    std::default_random_engine generator;
    generator.seed((int)time(0));
    std::normal_distribution<double> distribution(mu,sigma);
    
    const int n=nRows*nCols;
    for (int a=0; a<n; a++){
        r.payload->data[a]=distribution(generator);
    }
    
    return r;
}

Matrix& Matrix::operator+=(const Matrix& rhs){
    DIMCHECK(this, &rhs);
    
    int nRow=rows();
    int nCol=cols();
    
    //We cannot just add payload element by element,
    //because each of matrices can be transpose of other.
    for (int r=0; r<nRow; r++){
        IndexProxy myRow=row(r);
        IndexProxy otherRow=rhs.row(r);
        
        for (int c=0; c<nCol; c++){
            myRow[c]+=otherRow[c];
        }
    }
    
    return *this;
}

Matrix Matrix::operator+(const Matrix& rhs) const{
    DIMCHECK(this, &rhs);
    
    DEFINE_SAME_SIZE_MATRIX(result);
    
    int nRow=rows();
    int nCol=cols();
    
    for (int r=0; r<nRow; r++){
        IndexProxy myRow=row(r);
        IndexProxy otherRow=rhs.row(r);
        IndexProxy resultRow=result.row(r);
        
        for (int c=0; c<nCol; c++){
            resultRow[c]=myRow[c]+otherRow[c];
        }
    }
    
    return result;
}

Matrix& Matrix::operator+=(double rhs){
    const int n=payload->nRows*payload->nCols;
    for (int a=0; a<n; a++){
        payload->data[a]+=rhs;
    }
    
    return *this;
}

Matrix Matrix::operator+(double rhs) const{
    DEFINE_SAME_SIZE_MATRIX_NO_TRANSPOSE(result);
    result.isTranspose=isTranspose;
    
    int n=payload->nCols*payload->nRows;
    for (int a=0; a<n; a++){
        result.payload->data[a]=payload->data[a]+rhs;
    }
    
    return result;
}

Matrix operator+(double lhs, const Matrix& rhs){
    return rhs+lhs;
}




Matrix& Matrix::operator-=(const Matrix& rhs){
    DIMCHECK(this, &rhs);
    
    int nRow=rows();
    int nCol=cols();
    
    //We cannot just add payload element by element,
    //because each of matrices can be transpose of other.
    for (int r=0; r<nRow; r++){
        IndexProxy myRow=row(r);
        IndexProxy otherRow=rhs.row(r);
        
        for (int c=0; c<nCol; c++){
            myRow[c]-=otherRow[c];
        }
    }
    
    return *this;
}

Matrix Matrix::operator-(const Matrix& rhs) const{
    DIMCHECK(this, &rhs);
    
    DEFINE_SAME_SIZE_MATRIX(result);
    
    int nRow=rows();
    int nCol=cols();
    
    for (int r=0; r<nRow; r++){
        IndexProxy myRow=row(r);
        IndexProxy otherRow=rhs.row(r);
        IndexProxy resultRow=result.row(r);
        
        for (int c=0; c<nCol; c++){
            resultRow[c]=myRow[c]-otherRow[c];
        }
    }
    
    return result;
}

Matrix& Matrix::operator-=(double rhs){
    const int n=payload->nRows*payload->nCols;
    for (int a=0; a<n; a++){
        payload->data[a]-=rhs;
    }
    
    return *this;
}

Matrix Matrix::operator-(double rhs) const{
    DEFINE_SAME_SIZE_MATRIX_NO_TRANSPOSE(result);
    result.isTranspose=isTranspose;
    
    int n=payload->nCols*payload->nRows;
    for (int a=0; a<n; a++){
        result.payload->data[a]=payload->data[a]-rhs;
    }
    
    return result;
}

Matrix operator-(double lhs, const Matrix& rhs){
    return rhs-lhs;
}


int Matrix::rows() const{
    if (isTranspose)
        return payload->nCols;
    else
        return payload->nRows;
}

int Matrix::cols() const{
    if (isTranspose)
        return payload->nRows;
    else
        return payload->nCols;
}

Matrix Matrix::transpose() const{
    Matrix result(*this);
    result.isTranspose = !result.isTranspose;
    return result;
}

double &Matrix::operator ()(int r, int c){
    if (isTranspose){
        return payload->data[payload->nCols * c + r];
    } else {
        return payload->data[payload->nCols * r + c];
    }
}

Matrix Matrix::operator* (const Matrix &other) const{
    assert(rows()==other.cols());
    
    int nR=rows();
    int nC=other.cols();
    Matrix result(nR, nC);
    
    int sumWidth=cols();
    
    for (int r=0; r<nR; r++){
        IndexProxy leftRow=row(r);
        for (int c=0; c<nC; c++){
            IndexProxy rightCol=other.col(c);
            
            double accu=0;
            for (int i=0; i<sumWidth; i++){
                accu+=leftRow[i]*rightCol[i];
            }
            
            result(r,c)=accu;
        }
    }
    
    return result;
}

Matrix &Matrix::operator*= (const Matrix &other){
    *this=(*this)*other;
    return *this;
}

Matrix Matrix::operator* (const double other) const{
    DEFINE_SAME_SIZE_MATRIX_NO_TRANSPOSE(result);
    result.isTranspose=isTranspose;
    
    int n=payload->nCols*payload->nRows;
    for (int a=0; a<n; a++){
        result.payload->data[a]=payload->data[a]*other;
    }
    
    return result;
}

Matrix &Matrix::operator*= (const double other){
    int n=payload->nCols*payload->nRows;
    for (int a=0; a<n; a++){
        payload->data[a]*=other;
    }
    
    return *this;
}

Matrix operator*(double lhs, const Matrix& rhs){
    return rhs*lhs;
}

Matrix Matrix::operator/ (const double other) const{
    DEFINE_SAME_SIZE_MATRIX_NO_TRANSPOSE(result);
    result.isTranspose=isTranspose;
    
    int n=payload->nCols*payload->nRows;
    for (int a=0; a<n; a++){
        result.payload->data[a]=payload->data[a]/other;
    }
    
    return result;
}

Matrix &Matrix::operator/= (const double other){
    int n=payload->nCols*payload->nRows;
    for (int a=0; a<n; a++){
        payload->data[a]/=other;
    }
    
    return *this;
}

Matrix Matrix::rowwiseAdd(const Matrix &other) const{
    assert(other.rows()==1 && cols()==other.cols());
    
    DEFINE_SAME_SIZE_MATRIX(result);
    
    int nR=rows();
    int nC=cols();
    
    for (int r=0; r<nR; r++){
        IndexProxy a=row(r);
        IndexProxy b=other.row(0);
        IndexProxy res=result.row(r);
        
        for (int c=0; c<nC; c++){
            res[c]=a[c]+b[c];
        }
    }
    
    return result;
}

Matrix Matrix::colwiseAdd(const Matrix &other) const{
    assert(other.cols()==1 && rows()==other.rows());
    
    DEFINE_SAME_SIZE_MATRIX(result);
    
    int nR=rows();
    int nC=cols();
    
    for (int c=0; c<nC; c++){
        IndexProxy a=col(c);
        IndexProxy b=other.col(0);
        IndexProxy res=result.col(c);
        
        for (int r=0; r<nR; r++){
            res[r]=a[r]+b[r];
        }
    }
    
    return result;
    
}

Matrix Matrix::rowwiseSubtract(const Matrix &other) const{
    assert(other.rows()==1 && cols()==other.cols());
    
    DEFINE_SAME_SIZE_MATRIX(result);
    
    int nR=rows();
    int nC=cols();
    
    for (int r=0; r<nR; r++){
        IndexProxy a=row(r);
        IndexProxy b=other.row(0);
        IndexProxy res=result.row(r);
        
        for (int c=0; c<nC; c++){
            res[c]=a[c]-b[c];
        }
    }
    
    return result;
}

Matrix Matrix::colwiseSubtract(const Matrix &other) const{
    assert(other.cols()==1 && rows()==other.rows());
    
    DEFINE_SAME_SIZE_MATRIX(result);
    
    int nR=rows();
    int nC=cols();
    
    for (int c=0; c<nC; c++){
        IndexProxy a=col(c);
        IndexProxy b=other.col(0);
        IndexProxy res=result.col(c);
        
        for (int r=0; r<nR; r++){
            res[r]=a[r]-b[r];
        }
    }
    
    return result;
}

Matrix Matrix::colsum() const{
    int nR=rows();
    int nC=cols();
    
    Matrix result=Matrix::Zeros(1, nC);
    
    for (int r=0; r<nR; r++){
        IndexProxy thisrow=row(r);
        for (int c=0; c<nC; c++){
            result.payload->data[c] += thisrow[c];
        }
    }
    
    return result;
}

Matrix Matrix::rowsum() const{
    int nR=rows();
    int nC=cols();
    
    Matrix result=Matrix::Zeros(1, nC);
    
    for (int c=0; c<nC; c++){
        IndexProxy thiscol=col(c);
        for (int r=0; r<nR; r++){
            result.payload->data[r] += thiscol[r];
        }
    }
    
    return result;
}


void Matrix::save(std::ofstream &file) const{
    int nR=rows();
    int nC=cols();
    
    file.write((char*)&nR, sizeof(nR));
    file.write((char*)&nC, sizeof(nC));
    
    for (int r=0; r<nR; r++){
        IndexProxy thisrow=row(r);
        
        for (int c=0; c<nC; c++){
            double w=thisrow[c];
            file.write((char*)&w, sizeof(w));
        }
    }
}

void Matrix::load(std::ifstream &file){
    int nR;
    int nC;
    
    file.read((char*)&nR, sizeof(nR));
    file.read((char*)&nC, sizeof(nC));
    
    if (payload==NULL || nR!=rows() || nC!=cols()){
        releasePayload();
        payload=MatrixPool::allocate(nR, nC);
    }
    
    for (int r=0; r<nR; r++){
        IndexProxy thisrow=row(r);
        
        for (int c=0; c<nC; c++){
            double w;
            file.read((char*)&w, sizeof(w));
            thisrow[c]=w;
        }
    }
}

Matrix Matrix::elementwise(double (*transform)(double d)) const{
    DEFINE_SAME_SIZE_MATRIX_NO_TRANSPOSE(result);
    result.isTranspose=isTranspose;
    
    int n=payload->nRows*payload->nCols;
    for (int a=0; a<n; a++){
        result.payload->data[a]=transform(payload->data[a]);
    }
    return result;
}

Matrix Matrix::colmean() const{
    return colsum()/cols();
}

Matrix Matrix::rowmean() const{
    return rowsum()/rows();
}

double Matrix::sum(){
    double accu=0;
    int n=payload->nCols*payload->nRows;
    for (int a=0; a<n; a++){
        accu+=payload->data[a];
    }
    
    return accu;
}

void Matrix::minMax(double &min, double &max) const{
    min=max=payload->data[0];
    
    int n=payload->nCols*payload->nRows;
    for (int a=0; a<n; a++){
        double d=payload->data[a];
        if (d<min)
            min=d;
        else if (d>max)
            max=d;
    }
}

