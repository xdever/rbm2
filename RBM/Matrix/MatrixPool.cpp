//
//  MatrixPool.cpp
//  RBM
//
//  Created by Csordás Róbert on 2015. 09. 03..
//  Copyright (c) 2015. Csordás Róbert. All rights reserved.
//

#include "MatrixPool.h"

int MatrixPool::historyLength=10;
std::list<MatrixPayload *> MatrixPool::deallocatedMatrices;

void MatrixPool::deallocate(MatrixPayload *data){
    //Put the matrix in the pool.
    //If the pool has to many elements, remove the last.
    
    if (deallocatedMatrices.size()>historyLength){
        delete deallocatedMatrices.back();
        deallocatedMatrices.pop_back();
    }
    
    deallocatedMatrices.push_front(data);
}

MatrixPayload *MatrixPool::allocate(int nRows, int nCols){
    //Check the list if it has the right sized element. If yes, return it. If not,
    //allocate a new one.
    
    for (std::list<MatrixPayload *>::iterator it=deallocatedMatrices.begin();
         it != deallocatedMatrices.end(); ++it){
        
        if ((*it)->nRows == nRows && (*it)->nCols == nCols){
            MatrixPayload *result = *it;
            deallocatedMatrices.erase(it);
            
            result->referenceCounter=1;
            
            return result;
        }
    }
    
    return new MatrixPayload(nRows, nCols);
}

void MatrixPool::cleanup(){
    //Deallocate all payloads left in history.
    for (std::list<MatrixPayload *>::iterator it=deallocatedMatrices.begin();
         it != deallocatedMatrices.end(); ++it){
        
        delete *it;
    }
    
    deallocatedMatrices.clear();
}