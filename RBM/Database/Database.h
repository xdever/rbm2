//
//  Database.h
//  RBM
//
//  Created by Csordás Róbert on 2015. 09. 04..
//  Copyright (c) 2015. Csordás Róbert. All rights reserved.
//

#ifndef RBM_Database_h
#define RBM_Database_h

#include <stdint.h>
#include "Matrix.h"

class Database{
public:
    virtual uint32_t count() const=0;
    virtual uint32_t categoryCount() const=0;
    virtual Matrix getSampleFromAllCategories(int samplePerCategory) const=0;
    virtual const Matrix &operator[] (int index) const=0;
};



#endif
