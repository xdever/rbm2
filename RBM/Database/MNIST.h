//
//  MNIST.h
//  RBM
//
//  Created by Csordás Róbert on 2015. 09. 04..
//  Copyright (c) 2015. Csordás Róbert. All rights reserved.
//

#ifndef __RBM__MNIST__
#define __RBM__MNIST__

#include <stdio.h>
#include <vector>
#include "Database.h"

class MNIST : public Database
{
private:
    uint32_t cnt;
    Matrix *images;
    uint8_t *labels;
    
    std::vector <int>  samplesByCategory[10];
    
    void clear();
    void loadImage(const std::string &imageFile, uint8_t threshold);
    void loadLabels(const std::string &labelFile);
    
    static void fixEndian(uint32_t *data, int cnt);
    void initCategories();
public:
    MNIST();
    ~MNIST();
    
    virtual uint32_t count() const;
    void open(const std::string &imageFile, const std::string &labelFile, uint8_t threshold=50);
    virtual Matrix getSampleFromAllCategories(int samplePerCategory) const;
    virtual uint32_t categoryCount() const;
    virtual const Matrix &operator[] (int index) const;
};

#endif /* defined(__RBM__MNIST__) */
