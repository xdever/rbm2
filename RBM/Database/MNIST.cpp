//
//  MNIST.cpp
//  RBM
//
//  Created by Csordás Róbert on 2015. 09. 04..
//  Copyright (c) 2015. Csordás Róbert. All rights reserved.
//

#include "MNIST.h"
#include <arpa/inet.h>
#include <fstream>
#include <iostream>
#include <assert.h>

MNIST::MNIST()
{
    cnt=0;
    images=NULL;
    labels=NULL;
}


void MNIST::clear(){
    if (images)
        delete [] images;
    
    if (labels)
        delete [] labels;
    
    cnt=0;
}

MNIST::~MNIST()
{
    clear();
}

uint32_t MNIST::count() const{
    return cnt;
}

void MNIST::fixEndian(uint32_t *data, int cnt){
    for (int a=0; a<cnt; a++){
        data[a]=ntohl(data[a]);
    }
}

void MNIST::loadImage(const std::string &imageFile, uint8_t threshold){
    struct imageHeader{
        uint32_t magic;
        uint32_t cnt;
        uint32_t w;
        uint32_t h;
    } __attribute__((packed));
    
    std::ifstream input(imageFile.c_str());
    if (!input.is_open()){
        std::cerr<<"Failed to open file '"<<imageFile<<"'!"<<std::endl;
        exit(-1);
    }
    
    imageHeader header;
    input.read((char*)&header, sizeof(header));
    
    fixEndian((uint32_t*)&header, sizeof(header)/4);
    
    assert(header.magic==0x00000803);
    assert(header.w==28 && header.h==28);
    
    cnt=header.cnt;
    
    
    std::cout<<"Loaded "<<cnt<<" images"<<std::endl;
    
    images=new Matrix[cnt];
    
    for (int a=0; a<cnt; a++){
        char data[28*28];
        input.read((char*)data, 28*28);
        
        images[a]=Matrix::RowVector(28*28);
        for (int pixel=0; pixel<28*28; pixel++){
            images[a](0,pixel) = data[pixel] > threshold ? 1.0 : 0.0;
        }
    }
    
    input.close();
}

void MNIST::loadLabels(const std::string &labelFile){
    struct labelHeader{
        uint32_t magic;
        uint32_t cnt;
    } __attribute__((packed));
    
    std::ifstream input(labelFile.c_str());
    if (!input.is_open()){
        std::cerr<<"Failed to open file '"<<labelFile<<"'!"<<std::endl;
        exit(-1);
    }
    
    labelHeader header;
    input.read((char*)&header, sizeof(header));
    
    fixEndian((uint32_t*)&header, sizeof(header)/4);
    
    assert(header.magic==0x00000801);
    if (header.cnt!=cnt){
        std::cerr<<"Image and label file size doesn't match ("<<header.cnt<<"!="<<cnt<<")"<<std::endl;
        exit(-1);
    }
    
    labels=new uint8_t[cnt];
    input.read((char*)labels, cnt);
    
    input.close();
}

void MNIST::open(const std::string &imageFile, const std::string &labelFile, uint8_t threshold){
    loadImage(imageFile,threshold);
    loadLabels(labelFile);
    initCategories();
}

void MNIST::initCategories(){
    for (int a=0; a<10; a++)
        samplesByCategory[a].clear();
    
    for (uint32_t a=0; a<cnt; a++){
        samplesByCategory[labels[a]].push_back(a);
    }
}


Matrix MNIST::getSampleFromAllCategories(int samplePerCategory) const{
    Matrix dataset(samplePerCategory*10, 28*28);
    
    int currRow=0;
    for (int num=0; num<10; num++){
        for (int sample=0; sample<samplePerCategory; sample++){
            Matrix::IndexProxy destRow=dataset[sample];
            
            Matrix &img=images[samplesByCategory[num][rand() % samplesByCategory[num].size()]];
            Matrix::IndexProxy srcRow=img[0];
            for (int pix=0; pix<28*28; pix++){
                destRow[pix]=srcRow[pix];
            }
            
            currRow++;
        }
    }
    
    return dataset;
}

uint32_t MNIST::categoryCount() const{
    return 10;
}

const Matrix &MNIST::operator[] (int index) const{
    return images[index];
}
