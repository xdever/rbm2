//
//  RBMState.cpp
//  RBM
//
//  Created by Csordás Róbert on 2015. 09. 04..
//  Copyright (c) 2015. Csordás Róbert. All rights reserved.
//

#include "RBMState.h"

#include "RBMState.h"
#include <fstream>


RBMState::RBMState(int nVisible, int nHidden, RBMState const *oldState){
    if (oldState){
        (*this)=*oldState;
    } else {
        weights = Matrix(nVisible,nHidden);
        
        visibleBias = Matrix::Zeros(1, nVisible);
        hiddenBias = Matrix::Zeros(1, nHidden);
        
        this->nVisible=nVisible;
        this->nHidden=nHidden;
        
        weightDecay=0.00001;
        momentum=0.8;
        sparsityAverageCoeff=0.98;
        sparsity=0.03;
        sparsityCost=0.001;
    }
}

void RBMState::save(std::ofstream &file) const{
    weights.save(file);
    visibleBias.save(file);
    hiddenBias.save(file);
    
    file.write((const char*)&weightDecay, sizeof(weightDecay));
    file.write((const char*)&momentum, sizeof(momentum));
    file.write((const char*)&sparsityAverageCoeff, sizeof(sparsityAverageCoeff));
    file.write((const char*)&sparsity, sizeof(sparsity));
    file.write((const char*)&sparsityCost, sizeof(sparsityCost));
}

void RBMState::load(std::ifstream &file){
    weights.load(file);
    visibleBias.load(file);
    hiddenBias.load(file);
    
    file.read((char*)&weightDecay, sizeof(weightDecay));
    file.read((char*)&momentum, sizeof(momentum));
    file.read((char*)&sparsityAverageCoeff, sizeof(sparsityAverageCoeff));
    file.read((char*)&sparsity, sizeof(sparsity));
    file.read((char*)&sparsityCost, sizeof(sparsityCost));
}


RBMState::RBMState(const std::string filename){
    load(filename);
}

RBMState::RBMState(std::ifstream &file){
    load(file);
}

RBMState& RBMState::operator=(const RBMState& other){
    weights=other.weights;
    visibleBias=other.visibleBias;
    hiddenBias=other.hiddenBias;
    nVisible=other.nVisible;
    nHidden=other.nHidden;
    weightDecay=other.weightDecay;
    
    
    return *this;
}

void RBMState::save(const std::string filename) const{
    std::ofstream file(filename);
    
    if (!file.is_open()){
        throw "Failed to open file.";
    }
    
    save(file);
}
void RBMState::load(const std::string filename){
    std::ifstream file(filename);
    
    if (!file.is_open()){
        throw "Failed to open file.";
    }
    
    load(file);
}
