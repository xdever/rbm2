//
//  RBM.cpp
//  RBM
//
//  Created by Csordás Róbert on 2015. 09. 04..
//  Copyright (c) 2015. Csordás Róbert. All rights reserved.
//

#include "RBM.h"

#include <iostream>
#include <functional>
#include "RBM.h"
#include <math.h>
#include <fstream>
#include <random>
//#include "RBMVisualizer.h"

RBM::RBM(int nVisible, int nHidden, RBMState const *initialState):
    state(nVisible, nHidden, initialState)
{
    this->nHidden=state.nHidden;
    this->nVisible=state.nVisible;
    
    resetTrainTemporaryState();
}


void RBM::initWeightMatrix(){
    //Geoffrey Hinton suggests intializing weights from normal distribution
    //with mean of 0 and standard deviation of 0.01
    
    state.weights=Matrix::NormalRandom(nVisible, nHidden, 0, 0.01);
}


void RBM::initState(const Database &db){
    initBias(db);
    initWeightMatrix();
    
    resetTrainTemporaryState();
}

RBM::~RBM()
{
    
}

static double logistic(double val){
    return 1.0/(1.0 + exp(-val));
}

Matrix RBM::logistic(const Matrix &mat) {
    //Run ::logistic on every element of the matrix
    return mat.elementwise(::logistic);
}

static double randomActivate(double probability){
    return (((double)rand())/((double)RAND_MAX)) <= probability ? 1.0 : 0;
}

Matrix RBM::activate(const Matrix &mat){
    //Run ::randomActivate on every element of the matrix
    return mat.elementwise(::randomActivate);
}

void RBM::vis2hid(const Matrix &data, Matrix &probability, Matrix &outState) const{
    probability=logistic((data * state.weights).rowwiseAdd(state.hiddenBias));
    outState=activate(probability);
}

void RBM::hid2vis(const Matrix &data, Matrix &probability, Matrix &outState) const{
    probability=logistic((data * state.weights.transpose()).rowwiseAdd(state.visibleBias));
    outState=activate(probability);
}

//Hinton: However, it is common to use the probability, pi, instead of sampling a binary value.
//This is not nearly as problematic as using probabilities for the data-driven hidden states
//and it reduces sampling noise thus allowing faster learning.
#define USE_VISIBLE_PROBABILITIES

static double removeNegative(double d){
    if (d<0.0)
        return 0.0;
    else
        return d;
}

static double sqr(double d){
    return d*d;
}

double RBM::train(const Matrix &data, int CDn, double learningRate, Matrix *hiddenActivation){
    auto nData=data.rows();
    
    Matrix firstHiddenProbability;
    Matrix firstHiddenState;
    vis2hid(data, firstHiddenProbability, firstHiddenState);
    
    Matrix newVisibleProb;
    Matrix newVisibleState;
    Matrix newHiddenProb=firstHiddenProbability;
    Matrix newHiddenState=firstHiddenState;
    
    Matrix errorReference;
    
    if (hiddenActivation){
        *hiddenActivation=firstHiddenState;
    }
    
    //Do Gibbs sampling CDn times.
    for (int i=0; i<CDn; i++){
        hid2vis(newHiddenState, newVisibleProb, newVisibleState);
#ifdef USE_VISIBLE_PROBABILITIES
        vis2hid(newVisibleProb, newHiddenProb, newHiddenState);
#else
        vis2hid(newVisibleState, newHiddenProb, newHiddenState);
#endif
        
        //Save first visible reconstruction for error calculation
        if (i==0)
            errorReference=newVisibleProb;
    }
    
    //Calculate the actual weight correction
#ifdef USE_VISIBLE_PROBABILITIES
    Matrix correction= learningRate * (data.transpose() * firstHiddenProbability - newVisibleProb.transpose() * newHiddenProb)/nData;
#else
    Matrix correction= learningRate * (data.transpose() * firstHiddenProbability - newVisibleState.transpose() * newHiddenProb)/nData;
#endif
    
    //Momentum
    lastCorrection = lastCorrection * state.momentum + (1.0-state.momentum)*correction;
    state.weights += lastCorrection;
    
    //Weight decay
    state.weights -= state.weights * learningRate * state.weightDecay;
    
    //Update biases
#ifdef USE_VISIBLE_PROBABILITIES
    state.visibleBias += learningRate * ((data-newVisibleProb).colsum())/nData;
#else
    state.visibleBias += learningRate * ((data-newVisibleState).colsum())/nData;
#endif
    state.hiddenBias += learningRate * ((firstHiddenProbability-newHiddenProb).colsum())/nData;
    
    //Sparsity
    meanActivity = meanActivity * (state.sparsityAverageCoeff) + firstHiddenProbability.colmean()*(1.0-state.sparsityAverageCoeff);
    
    Matrix sparsityCorrection = learningRate * state.sparsityCost * (meanActivity - state.sparsity);
    //Only decrease activation,never increase
    sparsityCorrection=sparsityCorrection.elementwise(::removeNegative);
    
    state.weights=state.weights.rowwiseSubtract(sparsityCorrection);
    state.hiddenBias -= sparsityCorrection;
    
    //Calculate error
    return (data - errorReference).elementwise(::sqr).rowsum().elementwise(::sqrt).sum()/nData;
}

double RBM::trainStep(const Database &db, int batchSize, double learningRate, int cd){
    //auto data=constructMinibatch(db, batchSize);
    //Geoffrey Hinton suggsets taking 1 samples for all categories (if there are few categories)
    //for a minibatch.
    int catCount=db.categoryCount();
    Matrix data=db.getSampleFromAllCategories((batchSize+catCount-1)/catCount);
    
    double error;
    
    //Hinton suggests that the learning rate should be around 1e-3 of the order of magnitudes of the weights
    //learningRate=0.001 * state.weights.norm();
    
    // std::cout<<(this->learningRate)<<std::endl;
    error=train(data, cd, learningRate);
    
    return error;
}


static double logWeightInit(double p){
    return log(p/(1.0-p));
}

void RBM::initBias(const Database &db){
    //Geoffrey Hinton suggests initializing biases log(p/(1-p)),
    //where p is the proportion of the pixel turned on in training
    //data
    Matrix hist=Matrix::Zeros(1,nVisible);
    int c=db.count();
    
    for (int a=0; a<c; a++){
        hist+=db[a];
    }
    
    hist/=c;
    state.visibleBias=hist.elementwise(::logWeightInit);
}

Matrix RBM::sample(const Matrix &data, int nSteps, bool startWithHidden, Matrix *activation) const{
    //Don't worry about matrix allocations, it doesn't do anything here.
    Matrix hiddenState, visibleState, visibleProbability, hiddenProbability;
    
    if (startWithHidden){
        hid2vis(data, visibleProbability, visibleState);
    } else {
        visibleState=data;
    }
    
    for (int step=0; step<nSteps; step++){
        vis2hid(visibleState, hiddenProbability, hiddenState);
        hid2vis(hiddenState, visibleProbability, visibleState);
    }
    
    if (activation)
        *activation = visibleState;
    
    return visibleProbability;
}

void RBM::resetTrainTemporaryState(){
    lastCorrection=Matrix::Zeros(nVisible,nHidden);
    meanActivity=Matrix::Zeros(1, nHidden);
}

int RBM::getVisibleSize() const{
    return nVisible;
}

int RBM::getHiddenSize(int layer) const{
    (void)layer;
    return nHidden;
}

void RBM::save(const std::string filename) const{
    state.save(filename);
}


void RBM::load(const std::string filename){
    RBMState state(filename);
    this->state=state;
    resetTrainTemporaryState();
}

std::vector<Matrix> RBM::plotWeights(int nInRow, int weightImageW) const{
    std::vector<Matrix> result;
    return result;
  //  return RBMVisualizer::plotWeights(*this, nInRow, weightImageW);
}

int RBM::getLayerCount() const{
    return 1;
}
