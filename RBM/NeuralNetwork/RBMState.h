//
//  RBMState.h
//  RBM
//
//  Created by Csordás Róbert on 2015. 09. 04..
//  Copyright (c) 2015. Csordás Róbert. All rights reserved.
//

#ifndef __RBM__RBMState__
#define __RBM__RBMState__

#include <string>
#include "Matrix.h"

class RBMState{
    friend class RBM;
    friend class RBMVisualizer;
private:
    Matrix weights;
    Matrix visibleBias;
    Matrix hiddenBias;
    int nVisible;
    int nHidden;
    
    double weightDecay;
    double momentum;
    double sparsity;
    double sparsityAverageCoeff;
    double sparsityCost;
    
public:
    RBMState(const std::string filename);
    RBMState(std::ifstream &file);
    RBMState(int nVisible, int nHidden, RBMState const *oldState=NULL);
    RBMState& operator=(const RBMState& other);
    
    void save(const std::string filename) const;
    void load(const std::string filename);
    
    void save(std::ofstream &file) const;
    void load(std::ifstream &file);
};


#endif /* defined(__RBM__RBMState__) */
