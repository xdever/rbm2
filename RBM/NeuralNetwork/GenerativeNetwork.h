//
//  GenerativeNetwork.h
//  RBM
//
//  Created by Csordás Róbert on 2015. 09. 04..
//  Copyright (c) 2015. Csordás Róbert. All rights reserved.
//

#ifndef RBM_GenerativeNetwork_h
#define RBM_GenerativeNetwork_h

#include "Matrix.h"
#include "Database.h"
#include <string>
#include <vector>

class GenerativeNetwork{
public:
    virtual ~GenerativeNetwork(){};
    virtual double trainStep(const Database &db, int batchSize=10, double learningRate=0.1, int cd=1)=0;
    virtual Matrix sample(const Matrix &data, int nSteps=1000, bool startWithHidden=false, Matrix *activation=NULL) const=0;
    
    virtual int getVisibleSize() const=0;
    virtual int getHiddenSize(int layer=-1) const=0;
    
    virtual int getLayerCount() const=0;
    
    virtual void initState(const Database &db)=0;
    
    virtual void save(const std::string filename) const=0;
    virtual void load(const std::string filename)=0;
    
    virtual std::vector<Matrix> plotWeights(int nInRow, int weightImageW) const=0;
};

#endif
