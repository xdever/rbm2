//
//  RBM.h
//  RBM
//
//  Created by Csordás Róbert on 2015. 09. 04..
//  Copyright (c) 2015. Csordás Róbert. All rights reserved.
//

#ifndef __RBM__RBM__
#define __RBM__RBM__

#include "GenerativeNetwork.h"
#include "RBMState.h"
#include "Matrix.h"
#include "Database.h"

class RBM: public GenerativeNetwork
{
private:
    friend class RBMVisualizer;
    friend class DBN;
    
    RBMState state;
    
    int nVisible;
    int nHidden;
    
    //For the momentum
    Matrix lastCorrection;
    //For sparsity
    Matrix meanActivity;
    
    double train(const Matrix &data, int CDn=1, double learningRate=0.1, Matrix *hiddenActivation=NULL);
    
    static Matrix logistic(const Matrix &mat);
    static Matrix activate(const Matrix &mat);
    
    void vis2hid(const Matrix &data, Matrix &probability, Matrix &outState) const;
    void hid2vis(const Matrix &data, Matrix &probability, Matrix &outState) const;
    
    
    void resetTrainTemporaryState();
    
    void initWeightMatrix();
    void initBias(const Database &db);
public:
    RBM(int nVisible, int nHidden, RBMState const* initialState=NULL);
    virtual ~RBM();
    
    Matrix sample(const Matrix &data, int nSteps=1000, bool startWithHidden=false, Matrix *activation=NULL) const;
    
    
    double trainStep(const Database &db, int batchSize=10, double learningRate=0.1, int cd=1);
    
    void initState(const Database &db);
    
    int getVisibleSize() const;
    int getHiddenSize(int layer=-1) const;
    
    void save(const std::string filename) const;
    void load(const std::string filename);
    
    std::vector<Matrix> plotWeights(int nInRow, int weightImageW) const;
    
    int getLayerCount() const;
    
};

#endif /* defined(__RBM__RBM__) */
